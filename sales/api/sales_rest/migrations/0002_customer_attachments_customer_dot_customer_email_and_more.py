# Generated by Django 4.0.3 on 2023-11-06 20:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='attachments',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='customer',
            name='dot',
            field=models.IntegerField(null=True, unique=True),
        ),
        migrations.AddField(
            model_name='customer',
            name='email',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='customer',
            name='mc',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='sale',
            name='automobile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='automobile', to='sales_rest.automobilevo'),
        ),
    ]
