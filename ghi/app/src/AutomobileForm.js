import React, { useState, useEffect } from "react";


function AutomobileForm() {
    const [vin, setVin] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [year, setYear] = useState("");
    const [vehicle, setVehicle] = useState("");

    const [carriers, setCarriers] = useState([]);
    const fetchVehicles = async () => {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok) {
            const data = await response.json();
            setCarriers(data.customers);
        }
    };

    useEffect(() => {
        fetchVehicles();
    }, []);

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };
    const handleModelChange = (event) => {
        setModel(event.target.value);
    };
    const handleYearChange = (event) => {
        setYear(event.target.value);
    };
    const handleVehicleChange = (event) => {
        setVehicle(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model = model;
        data.year = year;
        data.vin = vin;
        console.log(data)

        const url = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setVin("");
            setManufacturer("");
            setModel("");
            setYear("");
        }
    };


    return (
        <>
            <div className="carousel-inner">
            <img
                src="https://us.moodmedia.com/wp-content/uploads/2021/10/4-pillars-feature.webp"
                className="opacity-50 w-100"
                alt="Background Image"
            />
            <div className="carousel-caption d-none d-md-block h-100 pt-5">
                <div className="d-flex align-items-center justify-content-center h-100">
                    <div className="bg-light p-4 text-dark">
                            <h1>Add a new automobile to inventory</h1>
                            <form onSubmit={handleSubmit} id="create-automobile-form">
                                <div className="form-floating mb-3">
                                    <input value={vin} onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                                    <label htmlFor="vin">VIN</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={model} onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                                    <label htmlFor="model">Model</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={year} onChange={handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                                    <label htmlFor="year">Year of manufacture</label>
                                </div>
                                <div className="mb-3">
                                    <select value={vehicle} onChange={handleVehicleChange} required name="vehicle" id="vehicle" className="form-select">
                                        <option value="">Choose a carrier</option>
                                        {carriers.map((carrier) => {
                                            return (
                                                <option key={ carrier.id } value={ carrier.id }>
                                                { carrier.name }
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            {/* </div> */}
        </>
    );
}

export default AutomobileForm;
