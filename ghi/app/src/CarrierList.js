import React, { useState, useEffect } from "react";


function CarrierList() {
    const [carriers, setCarriers] = useState([]);
    const [filteredCarrier, setFilteredCarrier] = useState([]);
    const [filterValue, setFilterValue] = useState("");

    const fetchCarriers = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setCarriers(data.customers);
        }
    }

    useEffect(() => {
        fetchCarriers()
    }, []);

    const fetchCarrierSearch = async () => {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok) {
            const data = await response.json();
            const carrierList = [];
            data.customers.map((carrier) => carrierList.push(carrier.dot));
            setFilteredCarrier(carrierList);
        }
    };

    useEffect(() => {
        fetchCarrierSearch();
    }, []);

    const handleFilterVal = (event) => {
        setFilterValue(event.target.value.toUpperCase());
    };

    const filteredCarriers = () => {
        if (filterValue === " ") {
            return carriers;
        } else {
            return carriers.filter((carrier) =>
                carrier.name.toUpperCase().includes(filterValue)
            );
        }
    };

    return (
        <>
        <div className="container" style={{ paddingTop: 40}}>
            <div className="pt-4">
                <h1 className="pb-2">List of Carriers</h1>
                    <form>
                        <div className="form mb-3">
                            <input value={filterValue} onChange={handleFilterVal} placeholder="Search by Name" name="filter-value" id="filter-value" className="form-control"/>
                        </div>
                    </form>
                <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>DOT Number</th>
                        <th>MC Number</th>
                        <th>Attachments</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredCarriers().map((carrier) => {
                        return (
                            <tr key={carrier.name}>
                                <td>{carrier.name}</td>
                                <td>{carrier.address}</td>
                                <td>{carrier.email}</td>
                                <td>{carrier.phone_number}</td>
                                <td>{carrier.dot}</td>
                                <td>{carrier.mc}</td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    </>
    );
}

export default CarrierList;
